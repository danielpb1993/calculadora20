package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private TextView textResult;
    private EditText num1;
    private EditText num2;
    private Button btAdd;
    private Button btSubst;
    private Button btMult;
    private Button btDiv;
    private Button btPower;
    private Button btCe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //hook = assignacions

        textResult = (TextView) findViewById(R.id.textResult);
        num1 = (EditText) findViewById(R.id.num1);
        num2 = (EditText) findViewById(R.id.num2);
        btAdd = (Button) findViewById(R.id.btAdd);
        btSubst = (Button) findViewById(R.id.btSubst);
        btMult = (Button) findViewById(R.id.btMult);
        btDiv = (Button) findViewById(R.id.btDiv);
        btPower = (Button) findViewById(R.id.btPower);
        btCe = (Button) findViewById(R.id.btCe);

        textResult.setText("0.0");

        btAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            + Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(result + "");
                }
            }
        });

        btSubst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            - Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(result + "");
                }
            }
        });

        btMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)  {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            * Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(result + "");
                }
            }
        });

        btDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)  {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else if (Double.parseDouble(num2.getText().toString().trim()) == 0) {
                    Toast.makeText(MainActivity.this, "Divide by zero impossible", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Double.parseDouble(num1.getText().toString().trim())
                            / Double.parseDouble(num2.getText().toString().trim());
                    textResult.setText(result + "");
                }
            }
        });

        btPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)  {
                if (num1.getText().toString().trim().isEmpty() ||
                        num2.getText().toString().trim().isEmpty()) {
                    Toast.makeText(MainActivity.this, "Enter a number", Toast.LENGTH_SHORT).show();
                } else {
                    double result = Math.pow(Double.parseDouble(num1.getText().toString().trim()),
                            Double.parseDouble(num2.getText().toString().trim()));
                    textResult.setText(result + "");
                }
            }
        });

        btCe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double result = 0.0;
                textResult.setText(result + "");
            }
        });
    }
}